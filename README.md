# lab-setup

1. Create google cloud account. Refer below video <br/>
    https://www.youtube.com/watch?v=DfvxxzithnE&t=3s
2. Create firewall rule to open all the ports
3. Install git bash in your system
4. generate ssh keys. upload the public key to google cloud
5. connect to your account through putty or some SSH clents.

```
ssh-keygen -f <file-name>

```
Note: Don't give any pass phrase

ssh -i [private key file]  [user-name]@[external-ip]