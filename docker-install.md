## Manual Instrunctions

1. We need to install Docker community edition in centos

2. Run the below command to install yum-utils

```
$ sudo yum install -y yum-utils device-mapper-persistent-data lvm2
```
3. Use the following command to set up the stable repository.
```
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```
4. Install docker
```
$ sudo yum install docker-ce docker-ce-cli containerd.io
```
5. Be default docker command runs with root user access. If you want run docker command with normal user access.
```
usermod -aG docker centos
```
6. You need to start and enable docker
```
$ sudo systemctl start docker
$ sudo sysytemctl enable docker
```
7. Logout and Login to the session.
8. Verify using below command
```
$docker ps
```
