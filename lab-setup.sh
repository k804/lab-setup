#!/bin/bash

USERID=$(id -u)

if [ $USERID != 0 ]; then
    echo -e "\e[31m Run with rooot user access \e[0m"
fi

yum install -y yum-utils device-mapper-persistent-data lvm2 -y

yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo -y

yum install docker-ce docker-ce-cli containerd.io -y

usermod -aG docker centos

systemctl start docker

systemctl enable docker

echo -e "\e[31m \n\t====You need to relogin====\e[0m"